<div class="header">
    <div>
      <p class="logo">technogy</p>
    </div>
    <div class="mail">
      <p>Send as a message</p>
      <p class="demo">demo@demo.com</p>
    </div>
    <div class="help">
      <p>Need help? Call Us:</p>
      <p class="demo">012 345 6789</p>
    </div>
    <div class="ikonice">
      <i class="fa fa-address-book-o"></i>
      <i class="fa fa-heart">0</i>
      <i class="fa fa-shopping-cart">0</i>
    </div>
  </div>